import { promises as fsp } from "fs";
import { equal } from "assert";
import { Builder, By, Capabilities, until } from "selenium-webdriver";
import { fun, asyncfun } from "./example.mjs";
import { get_db_memory, get_db_postgres } from "../database/database.mjs";
import { init_func } from "../database/init_db.mjs";
import { get_all_wycieczki, get_wycieczka } from "../database/queries.mjs";
import { system } from "selenium-webdriver/lib/proxy.js";
import sinon from "sinon";
import { app } from "../index.mjs";
import chai from "chai";
import chaiHttp from "chai-http";

async function takeScreenshot(driver, file) {
  const image = await driver.takeScreenshot();
  await fsp.writeFile(file, image, "base64");
}

describe("Function", () => {
  it("should output string equal to 'test'", () => {
    equal(fun(), "test");
  });
});

describe("Async function", () => {
  it("should output string equal to 'atest'", async () => {
    const a = await asyncfun();
    console.log(a);
    equal(a, "atest");
  });
});

describe("Selenium test", () => {
  const TIMEOUT = 10000;
  const driver = new Builder().withCapabilities(Capabilities.firefox()).build();

  before(async () => {
    await driver
      .manage()
      .setTimeouts({ implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT });
  });

  it("should go to google.com and check title", async () => {
    await driver.get("https://www.google.com");
    await takeScreenshot(driver, "test.png");
    const title = await driver.getTitle();
    console.log(title);
    equal(title, "Google");
  });

  after(() => driver.quit());
});

describe("Database tests", () => {
  let db, clock;
  let now = new Date("2022-05-27");

  before(async () => {
    db = await get_db_memory();
    await init_func(db);
  });

  beforeEach(() => {
    clock = sinon.useFakeTimers(now.getTime());
  });

  afterEach(() => {
    clock.restore();
  });

  after(async () => {
    await db.sequelize.drop();
    await db.sequelize.close();
  });

  it("test get_wycieczka", async () => {
    const pk = 1;
    let { wycieczka, zgloszenia } = await get_wycieczka(db, pk);

    equal(wycieczka.id, pk);
    for (const z of zgloszenia) {
      equal(z.WycieczkaId, pk);
    }
  });

  it("test get_all_wycieczki", async () => {
    let wycieczki = await get_all_wycieczki(db);

    equal(wycieczki.length, 2);
  });
});

describe("Page tests", () => {
  const TIMEOUT = 10000;
  const driver = new Builder().withCapabilities(Capabilities.firefox()).build();
  let db, server;

  before(async () => {
    db = await get_db_postgres();
    await init_func(db);
    server = app.listen();
    await driver
      .manage()
      .setTimeouts({ implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT });
  });

  after(async () => {
    driver.quit();
    server.close();
    await db.sequelize.drop();
    await db.sequelize.close();
  });

  it("Error page test", async () => {
    await driver.get("http://localhost:3000/error");
    const error_msg = await driver.findElement(By.id("error_msg"));
    const error_text = await error_msg.getText();

    equal(error_text, "Nie znaleziono strony o podanym adresie!");
  });

  it("Book test", async () => {
    let pk = 1;

    await driver.get("http://localhost:3000/book/" + pk);
    await driver.findElement(By.id("first_name")).sendKeys("imie");
    await driver.findElement(By.id("last_name")).sendKeys("nazwisko");
    await driver.findElement(By.id("phone")).sendKeys("123456789");
    await driver.findElement(By.id("email")).sendKeys("mail@gmail.com");
    await driver.findElement(By.id("n_people")).sendKeys("1");
    await driver.findElement(By.id("gdpr_permission")).click();
    await driver.findElement(By.id("submitid")).click();
    driver.wait(until.elementLocated(By.id("info")));
    const path = await driver.getCurrentUrl();

    equal(path, "http://localhost:3000/book-success/" + pk);
  });
});

describe("Page tests with chai", () => {
  let should = chai.should();
  chai.use(chaiHttp);
  let db;

  before(async () => {
    db = await get_db_postgres();
    await init_func(db);
  });

  after(async () => {
    await db.sequelize.drop();
    await db.sequelize.close();
  });

  it("Error page test", (done) => {
    chai.request(app).get("/error")
      .end((err, res) => {
        res.should.have.status(200);
        res.should.to.be.html
        res.text.should.contains("<p id=\"error_msg\">Nie znaleziono strony o podanym adresie!</p>");

        done();
      });
  });

  it("Book test", (done) => {
    let pk = 1;
    let book_data = {
      first_name: "imie",
      last_name: "nazwisko",
      phone: "123456789",
      email: "mail@gmail.com",
      n_people: "1"
    }

    chai.request(app)
      .post("/book/" + pk)
      .send(book_data)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.to.be.html
        res.text.should.contains("<p class=\"info\" id=\"info\">Z powodzeniem zarezerwowano wycieczkę!</p>");
        res.redirects[0].should.contains("/book-success/" + pk)

        done();
      });
  });
});
